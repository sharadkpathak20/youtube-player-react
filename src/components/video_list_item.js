import React from 'react';
import VideoDetail from './video_details'

const VideoListItem = ({video, onVideoSelect}) => {
    const imageUrl = video ? video.snippet.thumbnails.default.url : "";
    
    if(!video) {
        return <div>
            Loding...
        </div>
    } else{
        return (
             <li onClick={() => onVideoSelect(video)} className="list-group-item">
                <div className="video-list media">
                    <div className="media-left">
                        <img className="media-object" src={imageUrl} />    
                    </div>
                    <div className="media-body">
                    <div className="media-heading">{video.snippet.title}</div>
                    </div>
                </div>
             </li>
        );
    }
}

export default VideoListItem;
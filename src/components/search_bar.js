import React, { Component } from 'react';

class SearchBar extends Component {
    constructor(props,onSearchTermChange){
            super(props);

            this.state = { term : "" };
    }

    render() {
        return (
            <div>
                <div className="searchArea">
                    <input type="text" 
                        placeholder="Search"
                        value={this.state.term}
                        onChange={event => this.onInputChange( event.target.value)} />
                   
                </div>
                <div className="space-area">

                </div>
            </div>
        );
    };

    onInputChange(term) {
        this.setState({term });
        this.props.onSearchTermChange(term);
    }
}

export default SearchBar;
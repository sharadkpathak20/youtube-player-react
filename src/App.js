import _ from 'lodash';
import React, { Component } from 'react';
import SearchBar from './components/search_bar';
import YTSearch from 'youtube-api-search';
import VideoList from './components/video_list';
import VideoDetail from './components/video_details';

const API_KEY = "AIzaSyAAF7Va7558E1ngFftc7MrGCqnw4YKT2S0";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
                        videos : [],
                        selecteVideo : null,
                        searchVideo: null
                    };

       this,this.videoSearch('dolphin');

    }

    videoSearch(term) {
        YTSearch({ key: API_KEY, term: term}, (videos) => {
            this.setState({
                videos: videos, 
                selecteVideo: videos[0]
            });
        });
    }




    render() {

        const videoSearch = _.debounce((term) => {this.videoSearch(term)},300); //Wait function 3 sec
        
        return (
            <div className="container">
               <SearchBar 
                    onSearchTermChange={term => this.videoSearch(term)}
               /> 
               <VideoDetail video={this.state.selecteVideo} />
               <VideoList 
                onVideoSelect={selecteVideo => this.setState({selecteVideo})}
                videos={this.state.videos} 
               />
            </div> 
        );
    };
}

export default App;